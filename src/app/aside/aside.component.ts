import { Component, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {
  constructor(private stateService: StateService) {}

  ngOnInit() {}

  get userLoggedIn() {
    return this.stateService.userLoggedIn.asObservable();
  }
}
