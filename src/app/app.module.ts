import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';

import {
  ButtonsModule,
  TypeaheadModule,
  ProgressbarModule
} from 'ngx-bootstrap';

import { NavComponent } from './nav/nav.component';
import { RouterModule } from '@angular/router';
import { routes } from './app.routing';
import { SadPausalacComponent } from './sad-pausalac/sad-pausalac.component';
import { HappyPausalacComponent } from './happy-pausalac/happy-pausalac.component';
import { ProfileComponent } from './profile/profile.component';
import { AsideComponent } from './aside/aside.component';
import { PaymentComponent } from './payment/payment.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { InspektorComponent } from './inspektor/inspektor.component';
import { TransactionsComponent } from './transactions/transactions.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SadPausalacComponent,
    HappyPausalacComponent,
    ProfileComponent,
    LandingComponent,
    PaymentComponent,
    AsideComponent,
    RegistrationComponent,
    LoginComponent,
    AboutUsComponent,
    InspektorComponent,
    TransactionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    ProgressbarModule.forRoot(),
    TypeaheadModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
