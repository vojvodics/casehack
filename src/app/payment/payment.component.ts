import { Component, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  constructor(private stateService: StateService) {}

  ngOnInit() {}

  get poreskaObaveza() {
    return this.stateService.poreskaObaveza.asObservable();
  }

  get preduzetnik() {
    return this.stateService.preduzetnikName.getValue();
  }

  plati() {
    this.stateService.plati();
  }
}
