import { Component, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  constructor(private stateService: StateService) {}

  ngOnInit() {}

  get userLoggedIn() {
    return this.stateService.userLoggedIn.asObservable();
  }

  toggleClass() {}
}
