import { Routes } from '@angular/router';

import { ROUTES_CONFIG } from './routes.config';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { PaymentComponent } from './payment/payment.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { HappyPausalacComponent } from './happy-pausalac/happy-pausalac.component';
import { SadPausalacComponent } from './sad-pausalac/sad-pausalac.component';
import { InspektorComponent } from './inspektor/inspektor.component';
import { TransactionsComponent } from './transactions/transactions.component';

export const routes: Routes = [
  { path: ROUTES_CONFIG.HOME, component: LandingComponent },
  { path: ROUTES_CONFIG.PROFIL, component: ProfileComponent },
  { path: ROUTES_CONFIG.PAYMENT, component: PaymentComponent },
  { path: ROUTES_CONFIG.REGISTRACIJA_START, component: RegistrationComponent },
  { path: ROUTES_CONFIG.LOGIN, component: LoginComponent },
  { path: ROUTES_CONFIG.SUCCESS, component: HappyPausalacComponent },
  { path: ROUTES_CONFIG.SAD, component: SadPausalacComponent },
  { path: ROUTES_CONFIG.INSPEKTOR, component: InspektorComponent },
  { path: 'transactions', component: TransactionsComponent },
  { path: '', pathMatch: 'full', redirectTo: ROUTES_CONFIG.HOME },
  { path: '**', redirectTo: ROUTES_CONFIG.HOME }
];
