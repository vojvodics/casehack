import { Component, OnInit } from '@angular/core';

export interface Preduzetnik {
  pib: string;
  ime: string;
  delatnost: string;
  status: string;
}

const preduzetnici = [
  {
    pib: '1234567890',
    ime: 'Pausalac1',
    delatnost: 'frizer',
    status: 'Čeka rešenje',
  },
  {
    pib: '9245436754',
    ime: 'Pausalac2',
    delatnost: 'programer',
    status: 'Odobreno',
  },
  {
    pib: '3453475445',
    ime: 'Pausalac3',
    delatnost: 'moler',
    status: 'Čeka rešenje',
  },
  {
    pib: '3433375445',
    ime: 'Pausalac4',
    delatnost: 'pravnik',
    status: 'Čeka rešenje',
  },
  {
    pib: '4633223213',
    ime: 'Pausalac5',
    delatnost: 'frizer',
    status: 'Čeka rešenje',
  },
  {
    pib: '7976542324',
    ime: 'Pausalac6',
    delatnost: 'programer',
    status: 'Čeka rešenje',
  },
];

@Component({
  selector: 'app-inspektor',
  templateUrl: './inspektor.component.html',
  styleUrls: ['./inspektor.component.scss'],
})
export class InspektorComponent implements OnInit {
  inspektor;
  preduzetnici: Preduzetnik[] = preduzetnici;
  preduzetnikSearch = '';

  constructor() {}

  ngOnInit() {}

  filter() {
    this.preduzetnici = preduzetnici.filter(
      pred =>
        pred.ime.toLowerCase().includes(this.preduzetnikSearch.toLowerCase()) ||
        pred.delatnost
          .toLowerCase()
          .includes(this.preduzetnikSearch.toLowerCase()) ||
        pred.pib.toLowerCase().includes(this.preduzetnikSearch.toLowerCase()) ||
        pred.status
          .toLowerCase()
          .includes(this.preduzetnikSearch.toLowerCase()),
    );
  }
}
