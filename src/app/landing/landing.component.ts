import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '../routes.config';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public mockData = [
    'Pekar (šifra: 10.71)',
    'Nosač (šifra: 96.09)',
    'Lekar (šifra: 96.09)',
    'Gajennje pirinča (šifra: 01.12)'
  ];

  public selectedData = '';

  constructor(private router: Router) {}

  ngOnInit() {}

  postaniPausalac() {
    this.router.navigate([`/${ROUTES_CONFIG.REGISTRACIJA_START}`]);
  }
  ulogujSe() {
    this.router.navigate([`/${ROUTES_CONFIG.LOGIN}`]);
  }

  search() {
    // console.log(this.mockData, this.selectedData);
    // if (this.mockData.find(el => el === this.selectedData)) {
    this.router.navigate([`/${ROUTES_CONFIG.SUCCESS}`]);
    // } else {
    // this.router.navigate([`/${ROUTES_CONFIG.SAD}`]);
    // }
  }
}
