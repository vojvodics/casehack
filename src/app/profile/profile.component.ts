import { Component, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '../routes.config';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public showDetails = false;

  constructor(private stateService: StateService, private rotuer: Router) {}

  ngOnInit() {}

  get PIB() {
    return this.stateService.pib.getValue();
  }

  get preduzetnik() {
    return this.stateService.preduzetnikName.getValue();
  }

  get poreskaObaveza() {
    return this.stateService.poreskaObaveza.asObservable();
  }

  plati() {
    this.rotuer.navigate([`/${ROUTES_CONFIG.PAYMENT}`]);
  }
}
