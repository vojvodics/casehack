import { Component } from '@angular/core';
import { StateService } from './services/state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private stateService: StateService, private router: Router) {}

  get userLoggedIn() {
    return this.stateService.userLoggedIn.asObservable();
  }

  get isHomeProfile() {
    console.log(this.router.url.toString() === '/home');
    return this.router.url.toString() === '/home';
  }
}
