export const ROUTES_CONFIG = {
  HOME: 'home',
  PROFIL: 'profil',
  PAYMENT: 'plati',
  REGISTRACIJA_START: 'postani-pausalac',
  LOGIN: 'login',
  SUCCESS: 'success',
  SAD: 'sad',
  INSPEKTOR: 'inspektor'
};
