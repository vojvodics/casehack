import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  public userLoggedIn: BehaviorSubject<any> = new BehaviorSubject<any>(false);
  public pib: BehaviorSubject<any> = new BehaviorSubject<any>('1235234237');
  public preduzetnikName: BehaviorSubject<any> = new BehaviorSubject<any>(
    'Paušalko'
  );

  public poreskaObaveza: BehaviorSubject<any> = new BehaviorSubject<any>(
    30794.52452
  );

  constructor() {}

  public plati() {
    setTimeout(() => {
      this.poreskaObaveza.next(0.00001);
    }, 3000);
  }

  public login() {
    this.userLoggedIn.next(true);
  }
}
