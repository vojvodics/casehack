import { Component, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { Router } from '@angular/router';
import { ROUTES_CONFIG } from '../routes.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private stateService: StateService, private router: Router) {}

  ngOnInit() {}

  login() {
    setTimeout(() => {
      this.stateService.login();
      this.router.navigate([`/${ROUTES_CONFIG.PROFIL}`]);
    }, 500);
  }
}
