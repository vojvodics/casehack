import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-happy-pausalac',
  templateUrl: './happy-pausalac.component.html',
  styleUrls: ['./happy-pausalac.component.scss']
})
export class HappyPausalacComponent implements OnInit {
  public selectedData = '';
  public x = { lokacija: '', ulica: '' };
  public y = null;
  public xRes = null;
  public yRes = null;
  public opstina;
  public ulica;

  public mockData = [
    'Pekar (šifra: 10.71)',
    'Nosač (šifra: 96.09)',
    'Lekar (šifra: 96.09)',
    'Gajennje pirinča (šifra: 01.12)'
  ];
  constructor() {}

  ngOnInit() {}

  promeni() {
    if (this.selectedData === 'Pekar (šifra: 10.71)') {
      this.x = { lokacija: '', ulica: '' };
      this.y = null;
    } else if (false) {
      // this.x = null;
      // this.y = { godine: "", invaliditet: "" };
    }
  }

  promeni2() {
    if (this.x) {
      this.xRes = 24932;
      this.yRes = null;
    } else {
      this.yRes = 34932;
      this.xRes = null;
    }
  }
}
